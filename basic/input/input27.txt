#length 600
#start AGGAAAGAG
#probe 9
AAAACAAAA
AAAACTAAA
AAAAGAGTG
AAAATAAAA
AAAATGTAT
AAACAAAAT
AAACAAGGT
AAACCAACC
AAACTAAAA
AAACTAAAC
AAACTGAAA
AAAGAGCAA
AAAGAGTCC
AAAGAGTGG
AAAGATATC
AAAGCAGAG
AAAGCTGCA
AAATAAAAT
AAATAAGGC
AAATCAACC
AAATCTGGA
AAATCTTTT
AAATGTACT
AAATTCTGA
AACAAAATA
AACAAGGAT
AACAAGGTT
AACAGTTCT
AACCAACCG
AACCGAGCT
AACCTCCTC
AACTAAAAC
AACTAAACA
AACTCAGCC
AACTGAAAT
AACTGCTAC
AAGAAGGCC
AAGAGCAAC
AAGAGTCCA
AAGAGTGGG
AAGATATCT
AAGATCAAT
AAGATGAAA
AAGCAGAGA
AAGCCCCTC
AAGCTGCAA
AAGGATAAA
AAGGCCTGC
AAGGCGTAA
AAGGTTTCA
AAGTGCAAT
AAGTTGGAC
AATAAAATG
AATAAATCT
AATAAGGCG
AATAATAAA
AATAATGGA
AATCAACCT
AATCTCAAA
AATCTGGAG
AATCTTTTG
AATGATCAG
AATGGAAGA
AATGGATAA
AATGTACTA
AATGTGGGC
AATTCTGAT
AATTGCCAG
AATTGTGCT
ACAAAATAA
ACAACAGTT
ACAAGGATA
ACAAGGTTT
ACAATGTGG
ACACTGGAA
ACAGATTGG
ACAGGAATT
ACAGTTCTC
ACCAACCGA
ACCAGTGGC
ACCCTAGCT
ACCGAGCTT
ACCTCCTCC
ACCTGGGCC
ACTAAAACT
ACTAAACAA
ACTAAAGCT
ACTCAGCCC
ACTCTCATA
ACTGAAATG
ACTGCTACT
ACTGGAATA
AGAAAACAA
AGAAGAAGG
AGAAGGCCT
AGAGATAGA
AGAGCAACT
AGAGGAGCT
AGAGTCCAA
AGAGTGGGC
AGATAAACT
AGATAGATC
AGATATCTG
AGATCAATG
AGATCTTGC
AGATGAAAG
AGATTGGAA
AGCAACTGC
AGCAGAGAT
AGCAGCTGT
AGCCAGCAG
AGCCCAAAT
AGCCCAGTT
AGCCCCTCA
AGCCTCTCA
AGCTGCAAA
AGCTGGAAA
AGCTGTGGG
AGCTTCCTG
AGCTTGATC
AGCTTTGCT
AGCTTTTAG
AGGAAAGAG
AGGAAGTTG
AGGAATTGT
AGGAGCTGG
AGGAGCTTT
AGGATAAAC
AGGCAGAGG
AGGCCTGCC
AGGCGTAAA
AGGCTAAAT
AGGGAAAAG
AGGTCTGGA
AGGTTTCAA
AGTCCAAGT
AGTCTTGTG
AGTGACAAT
AGTGCAATA
AGTGCTTAG
AGTGGCTGC
AGTGGGCGC
AGTTCAGCT
AGTTCTCCC
AGTTGGACA
ATAAAATGT
ATAAACCAA
ATAAACTGA
ATAAATCTT
ATAAGATCA
ATAAGCCCC
ATAAGGCGT
ATAATAAAT
ATAATGGAA
ATACCTGGG
ATAGATCTT
ATAGGAAGT
ATAGGCAGA
ATAGGGAAA
ATATCTGTT
ATATGTGTG
ATCAACCTC
ATCAATGGA
ATCAGCCTC
ATCATGGTG
ATCTCAAAG
ATCTGAATG
ATCTGGAGA
ATCTGTTAT
ATCTTGCAG
ATCTTTTGG
ATGAAAGAT
ATGATCAGC
ATGGAAGAT
ATGGATAAG
ATGGTGGGA
ATGTACTAA
ATGTGGGCT
ATGTGTGGG
ATTCTGATA
ATTGCCAGG
ATTGGAATC
ATTGTGCTG
ATTTCATTT
ATTTGAAAT
CAAAATAAA
CAAAGAGTC
CAAATCAAC
CAAATTCTG
CAACAGTTC
CAACCGAGC
CAACCTCCT
CAACTCAGC
CAACTGCTA
CAAGGATAA
CAAGGTTTC
CAAGTGCAA
CAATAATAA
CAATGGATA
CAATGTGGG
CAATTGCCA
CACAACAGT
CACAGGAAT
CACCAGTGG
CACCCTAGC
CACTGGAAT
CAGAGATAG
CAGAGGAGC
CAGATTGGA
CAGCAGCTG
CAGCCCAGT
CAGCCTCTC
CAGCTGTGG
CAGCTTGAT
CAGGAATTG
CAGGTCTGG
CAGTCTTGT
CAGTGCTTA
CAGTGGCTG
CAGTTCAGC
CAGTTCTCC
CATACCTGG
CATAGGAAG
CATAGGCAG
CATGGTGGG
CATTTGAAA
CCAAATTCT
CCAACCGAG
CCAAGTGCA
CCAATTGCC
CCACAGGAA
CCAGCAGCT
CCAGGTCTG
CCAGTGGCT
CCAGTTCAG
CCATAGGCA
CCCAAATTC
CCCACAGGA
CCCAGTTCA
CCCCACAGG
CCCCCACAG
CCCCTCACC
CCCTAGCTT
CCCTCACCA
CCGAGCTTT
CCTAGCCCA
CCTAGCTTC
CCTCACCAG
CCTCCTAGC
CCTCCTCCT
CCTCTCACC
CCTGATAGG
CCTGCCATA
CCTGGGCCA
CGAGCTTTT
CGCTCTCTG
CGTAAACTA
CTAAAACTA
CTAAACAAG
CTAAAGCTG
CTAAATCTG
CTACTCTCA
CTAGCCCAA
CTAGCTTCC
CTCAAAGAG
CTCACCAGT
CTCACCCTA
CTCAGCCCA
CTCATACCT
CTCCCCCAC
CTCCTAGCC
CTCCTCCTA
CTCTCACCC
CTCTCATAC
CTCTCTGGA
CTCTGGAGA
CTGAAATGT
CTGAATGAT
CTGAGCCAG
CTGATAAGA
CTGATAGGG
CTGCAAATC
CTGCATAGG
CTGCCATAG
CTGCTACTC
CTGGAAAGC
CTGGAACAA
CTGGAATAA
CTGGAGAAA
CTGGAGATA
CTGGATATG
CTGGGCCAA
CTGGTGTGA
CTGTGGGGG
CTGTTATTT
CTTAGGCTA
CTTCCTGAT
CTTGATCAT
CTTGCAGTC
CTTGTGCAG
CTTTGCTGG
CTTTTAGTG
CTTTTGGCA
GAAAACAAA
GAAAAGAGT
GAAAGAGCA
GAAAGATAT
GAAAGCAGA
GAAATAAGG
GAAATGTAC
GAACAAGGA
GAAGAAGGC
GAAGATGAA
GAAGGCCTG
GAAGTTGGA
GAATAATGG
GAATCTCAA
GAATGATCA
GAATTGTGC
GACAATGTG
GACACTGGA
GACAGATTG
GAGAAAACA
GAGAAGAAG
GAGATAAAC
GAGATAGAT
GAGCAACTG
GAGCCAGCA
GAGCTGGAA
GAGCTTTGC
GAGCTTTTA
GAGGAGCTG
GAGGAGCTT
GAGTCCAAG
GAGTGGGCG
GATAAACCA
GATAAACTG
GATAAGATC
GATAAGCCC
GATAGATCT
GATAGGGAA
GATATCTGT
GATATGTGT
GATCAATGG
GATCAGCCT
GATCATGGT
GATCTGAAT
GATCTTGCA
GATGAAAGA
GATTGGAAT
GCAAATCAA
GCAACTGCT
GCAATAATA
GCACAACAG
GCAGAGATA
GCAGAGGAG
GCAGCTGTG
GCAGTCTTG
GCAGTGCTT
GCATAGGAA
GCCAATTGC
GCCAGCAGC
GCCAGGTCT
GCCATAGGC
GCCCAAATT
GCCCAGTTC
GCCCCTCAC
GCCTCTCAC
GCCTGCCAT
GCGCTCTCT
GCGTAAACT
GCTAAATCT
GCTACTCTC
GCTCTCTGG
GCTGAGCCA
GCTGCAAAT
GCTGCATAG
GCTGGAAAG
GCTGGAACA
GCTGGTGTG
GCTGTGGGG
GCTTAGGCT
GCTTCCTGA
GCTTGATCA
GCTTTGCTG
GCTTTTAGT
GGAAAAGAG
GGAAAGAGC
GGAAAGCAG
GGAACAAGG
GGAAGATGA
GGAAGTTGG
GGAATAATG
GGAATCTCA
GGAATTGTG
GGACACTGG
GGAGAAAAC
GGAGATAAA
GGAGCTGGA
GGAGCTTTG
GGAGGAGCT
GGATAAACC
GGATAAGCC
GGATATGTG
GGATCTGAA
GGCACAACA
GGCAGAGGA
GGCCAATTG
GGCCTGCCA
GGCGCTCTC
GGCGTAAAC
GGCTAAATC
GGCTGCATA
GGCTGGTGT
GGGAAAAGA
GGGAGGAGC
GGGATCTGA
GGGCCAATT
GGGCGCTCT
GGGCTGGTG
GGGGAGGAG
GGGGGAGGA
GGGTTGAGA
GGTCTGGAT
GGTGGGATC
GGTGTGACA
GGTTGAGAA
GGTTTCAAC
GTAAACTAA
GTACTAAAG
GTCCAAGTG
GTCTGGATA
GTCTTGTGC
GTGACAATG
GTGACAGAT
GTGCAATAA
GTGCAGTGC
GTGCTGAGC
GTGCTTAGG
GTGGCTGCA
GTGGGATCT
GTGGGCGCT
GTGGGCTGG
GTGGGGGAG
GTGGGTTGA
GTGTGACAG
GTGTGGGTT
GTTATTTCA
GTTCAGCTT
GTTCTCCCC
GTTGAGAAG
GTTGGACAC
GTTTCAACT
TAAAACTAA
TAAAATGTA
TAAACAAGG
TAAACCAAC
TAAACTAAA
TAAACTGAA
TAAAGCTGC
TAAATCTGG
TAAATCTTT
TAAGATCAA
TAAGCCCCT
TAAGGCGTA
TAATAAATC
TAATGGAAG
TACCTGGGC
TACTAAAGC
TACTCTCAT
TAGATCTTG
TAGCCCAAA
TAGCTTCCT
TAGGAAGTT
TAGGCAGAG
TAGGCTAAA
TAGGGAAAA
TAGTGACAA
TATCTGTTA
TATGTGTGG
TATTTCATT
TCAAAGAGT
TCAACCTCC
TCAACTCAG
TCAATGGAT
TCACCAGTG
TCACCCTAG
TCAGCCCAG
TCAGCCTCT
TCAGCTTGA
TCATACCTG
TCATGGTGG
TCATTTGAA
TCCAAGTGC
TCCCCCACA
TCCTAGCCC
TCCTCCTAG
TCCTGATAG
TCTCAAAGA
TCTCACCCT
TCTCATACC
TCTCCCCCA
TCTCTGGAG
TCTGAATGA
TCTGATAAG
TCTGGAGAA
TCTGGAGAT
TCTGGATAT
TCTGTTATT
TCTTGCAGT
TCTTGTGCA
TCTTTTGGC
TGAAAGATA
TGAAATAAG
TGAAATGTA
TGAATGATC
TGACAATGT
TGACAGATT
TGAGAAGAA
TGAGCCAGC
TGATAAGAT
TGATAGGGA
TGATCAGCC
TGATCATGG
TGCAAATCA
TGCAATAAT
TGCAGTCTT
TGCAGTGCT
TGCATAGGA
TGCCAGGTC
TGCCATAGG
TGCTACTCT
TGCTGAGCC
TGCTGGAAC
TGCTTAGGC
TGGAAAGCA
TGGAACAAG
TGGAAGATG
TGGAATAAT
TGGAATCTC
TGGACACTG
TGGAGAAAA
TGGAGATAA
TGGATAAGC
TGGATATGT
TGGCACAAC
TGGCTGCAT
TGGGATCTG
TGGGCCAAT
TGGGCGCTC
TGGGCTGGT
TGGGGGAGG
TGGGTTGAG
TGGTGGGAT
TGGTGTGAC
TGTACTAAA
TGTGACAGA
TGTGCAGTG
TGTGCTGAG
TGTGGGCTG
TGTGGGGGA
TGTGGGTTG
TGTGTGGGT
TGTTATTTC
TTAGGCTAA
TTAGTGACA
TTATTTCAT
TTCAACTCA
TTCAGCTTG
TTCATTTGA
TTCCTGATA
TTCTCCCCC
TTCTGATAA
TTGAAATAA
TTGAGAAGA
TTGATCATG
TTGCAGTCT
TTGCCAGGT
TTGCTGGAA
TTGGAATCT
TTGGACACT
TTGGCACAA
TTGTGCAGT
TTGTGCTGA
TTTAGTGAC
TTTCAACTC
TTTCATTTG
TTTGAAATA
TTTGCTGGA
TTTGGCACA
TTTTAGTGA
TTTTGGCAC
