import os
import sys

def read_input():
    length = int(input().rstrip().split()[1])
    start = input().rstrip().split()[1]
    probe = int(input().rstrip().split()[1])
    oligo = [input() for _ in range(length - probe + 1)]
    return length, start, probe, oligo

def input_files():
    directory = "./basic/input"
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        if os.path.isfile(f):
            print(f)
            with open(f, "r") as file:
                length, start, probe, oligo = read_file(file)
                file.close()
                oligoDict = gen_dict(oligo, probe)    
                end_path = hamilton(oligoDict, length-probe+1, oligo.index(start), [])
                print(createOligoPath(oligo, end_path))

def read_file(file):
    length = int(file.readline().rstrip().split()[1])
    start = file.readline().rstrip().split()[1]
    probe = int(file.readline().rstrip().split()[1])
    oligo = [file.readline() for _ in range(length - probe + 1)]
    oligo = [line[:-1] for line in oligo]
    return length, start, probe, oligo

def canBeStack(last_probe, next_probe, probe):
    return last_probe[len(last_probe)-probe+1:len(last_probe)] == next_probe[0:probe-1]

def gen_dict(oligo, probe):
    oligoDict = {key: None for key in range(len(oligo))}
    for i in range(len(oligoDict)):
        oligoDict[i] = list(filter(lambda j : canBeStack(oligo[i], oligo[j], probe) and i != j, oligoDict))
    return oligoDict

def hamilton(oligoDict, length, vertex, path):
    if vertex not in set(path):
        path.append(vertex)
        if len(path) == length:
            return path
        for nextVertex in oligoDict.get(vertex, []):
            possibleNextVertex = hamilton(oligoDict, length, nextVertex, [i for i in path])
            if possibleNextVertex is not None:
                return possibleNextVertex
    else:
        pass
    # dead end
    return None

def createOligoPath(oligo, end_path):
    end = ""
    for i in range(len(end_path)):
        end += oligo[end_path[i]] if i==0 else oligo[end_path[i]][-1]
    return end

if __name__ == '__main__':
    sys.setrecursionlimit(1000000)

    # files - testing version
    input_files()

    # input - hackerrank version
    # length, start, probe, oligo = read_input()
    # # print(length, start, probe, oligo)
    # oligoDict = gen_dict(oligo, probe)
    # end_path = hamilton(oligoDict, length-probe+1, oligo.index(start), [])
    # print(createOligoPath(oligo, end_path))